import 'package:flutter/material.dart';
import 'package:sugar_meter_flutter/app/sign_in/sign_in_page.dart';

void main() {
  runApp(SugarMeter());
}

class SugarMeter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sugar Meter',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: SignInPage(),
    );
  }
}
